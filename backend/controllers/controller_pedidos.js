const express =require('express');
const modeloCliente = require('../models/model_clientes');
const router =express.Router();
const modeloPedidos = require ('../models/model_pedidos');

router.get ('/listar', (req,res) => {
    modeloPedidos.find({}, function (docs,err)
        {
            if (!err)
            {
                res.send(docs);
            }
            else
            {
                res.send(err);
            }
        })
});

router.get ('/cargar/:id', (req,res) => {
    modeloPedidos.find({id:req.params.id}, function (docs,err)
        {
            if (!err)
            {
                res.send(docs);
            }
            else
            {
                res.send(err);
           }
        })
});

router.post('/agregar', (req,res) =>{
    const nuevoPedido = new modeloPedidos({
        id : req.body.id ,
        id_cliente : req.body.id_cliente,
        fecha : req.body.fecha,
        valor:req.body.valor,
        activo : req.body.activo    
    });

    nuevoPedido.save (function(err)
    {
        if(!err)
        {
            res.send("el nuevo pedido ha sido agregado");
        }
        else
        {
            res.send(err.stack);
        }
    })
});

router.post('/editar/:id', (req,res)=>{
    modeloPedidos.findOneAndUpdate({id:req.params.id},
        {
            id_cliente : req.body.id_cliente,
            fecha : req.body.fecha,
            valor:req.body.valor,
            activo : req.body.activo
        }, (err) =>
        {
            if(!err)
            {
                res.send("el pedido ha sido editado de forma correcta");

            }
            else
            {
                res.send(err.stack);
            }
        })
});

router.delete('borrar/:id', (req,res)=>{
    modeloPedidos.findOneAndDelete({id:req.params-id},
        (err) =>
        {
            if (!err)
            {
                res.send("el pedido se borro de forma correcta");
            }
            else
            {
                res.send(err.stack);
            }
        })
});

module.exports =router;    
    
