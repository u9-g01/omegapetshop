const express = require('express');
const modeloCliente = require('../models/model_clientes');
const router = express.Router();
const modeloClientes = require ('../models/model_clientes');


router.get('/listar', (req,res) => {
    modeloClientes.find({}, function (docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});


router.get ('/cargar/:id', (req,res) => {
    modeloCliente.find({id:req.params.id}, function (docs,err)
        {
            if (!err)
            {
                res.send(docs);
            }
            else
            {
                res.send(err);
           }
        })
});

router.post('/agregar', (req,res) =>{
    const nuevoCliente = new modeloCliente({
        id : req.body.id ,
        id_tipodocumento : req.body.id_tipodocumento,
        nombre : req.body.nombre,
        telefono : req.body.telefono,
        direccion : req.body.direccion,
        activo : req.body.activo
    });

    nuevoCliente.save (function(err)
    {
        if(!err)
        {
            res.send("el nuevo cliente ha sido agregado");
        }
        else
        {
            res.send(err.stack);
        }
    })
})

router.post('/editar/:id', (req,res)=>{
    modeloCliente.findOneAndUpdate({id:req.params.id},
        {
            id_tipodocumento : req.body.id_tipodocumento,
            nombre : req.body.nombre,
            telefono : req.body.telefono,
            direccion : req.body.direccion,
            activo : req.body.activo
        }, (err) =>
        {
            if(!err)
            {
                res.send("el cliente ha sido editado de forma correcta");

            }
            else
            {
                res.send(err.stack);
            }
        })
});

router.delete('borrar/:id', (req,res)=>{
    modeloCliente.findOneAndDelete({id:req.params-id},
        (err) =>
        {
            if (!err)
            {
                res.send("el registro de edito de forma corrwcta");
            }
            else
            {
                res.send(err.stack);
            }
        })
});

module.exports =router;